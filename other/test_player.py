#!/usr/bin/env python3

import sys
import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib

class Player(object):

    def on_message(self, bus, msg, user_data):
        t = msg.type

        if t == Gst.MessageType.EOS:
            print("End of stream")
            self.loop.quit()

        elif t == Gst.MessageType.ERROR:
            err, debug = msg.parse_error()
            print("Error: {0:s}".format(err.message))
            self.loop.quit()

        elif t == Gst.MessageType.STATE_CHANGED:
            if msg.src == self.pipeline:
                old, new, pending = msg.parse_state_changed()
                print("State changed: {0:s} {1:s} -> {2:s}".format(msg.src.name, Gst.Element.state_get_name(old), Gst.Element.state_get_name(new)))

        elif t == Gst.MessageType.STREAM_STATUS:
            print("Stream status ({0:s})".format(msg.src.name))

        elif t == Gst.MessageType.NEW_CLOCK:
            print("New clock ({0:s})".format(msg.src.name))
            clock = msg.parse_new_clock()
            print("clock time: {0:d}".format(clock.get_time()))

        else:
            # Unhandled message
            print("Received message: src={0} type={1}".format(msg.src.name, Gst.MessageType.get_name(msg.type)))
        
    def __init__(self):
        # initialisation
        Gst.init(sys.argv)
        
        # main loop
        self.loop = GLib.MainLoop.new(None, False)

        # create the pipeline
        #pipeline = Gst.Pipeline.new("pipeline")
        pipeline_str = "playbin uri=https://www.freedesktop.org/software/gstreamer-sdk/data/media/sintel_trailer-480p.webm"
        #pipeline_str = "videotestsrc ! video/x-raw,framerate=25/1,width=256,height=144 ! autovideosink audiotestsrc ! autoaudiosink"
        self.pipeline = Gst.parse_launch(pipeline_str)

        # message handler
        bus = self.pipeline.get_bus()
        #bus.add_watch(GLib.PRIORITY_DEFAULT, self.on_message, None)
        bus.add_signal_watch()
        bus.connect("message", self.on_message, None)

        # set the pipeline to PLAYING state
        self.pipeline.set_state(Gst.State.PLAYING)
        
        # iterate
        print("Running...")
        self.loop.run()

        # out of the main loop, clean up nicely
        print("Returned, stopping playback")
        self.pipeline.set_state(Gst.State.NULL)

if __name__ == '__main__':
    # to be able to use CTRL-C to quit
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    p = Player()
