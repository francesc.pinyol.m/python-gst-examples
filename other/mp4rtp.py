#!/usr/bin/env python3

# sdp:
# v=0
# c=IN IP4 234.1.2.3
# m=video 5004 RTP/AVP 96
# a=rtpmap:96 H264/90000
# a=fmtp:96 packetization-mode=1; sprop-parameter-sets=Z2QAHqzZQKAv+WEAAAMAAQAAAwAyDxYtlg==,aOvjyyLA; profile-level-id=64001E
# m=audio 5006 RTP/AVP 97
# a=rtpmap:97 MPEG4-GENERIC/44100/2
# a=fmtp:97 profile-level-id=1;mode=AAC-hbr;sizelength=13;indexlength=3;indexdeltalength=3; config=1210

import sys
import gi
gi.require_version('Gst', '1.0')
gi.require_version('GstSdp', '1.0')
from gi.repository import GLib, Gst, GstSdp

class Player(object):
            
    def build_sdp(self, rtpbin, address, port, sdp_path, session_name=None):
        """
        rtpbin: Gst.RtpBin element
        address: destination address
        port: initial destination port
        sdp_path: created file with SDP
        session_name: (optional) session name (s=...)
        """
        ret, sdp_message = GstSdp.SDPMessage.new()
        sdp_message.set_version('0')
        ttl = 64
        number_addresses = 1
        sdp_message.set_connection("IN", "IP4", address, ttl, number_addresses)
        if session_name:
            sdp_message.set_session_name(session_name)
        
        pads = rtpbin.iterate_pads()
        while True:
            ret, pad = pads.next()
            if ret==Gst.IteratorResult.OK:
                # only source pads
                if pad.direction != Gst.PadDirection.SRC:
                    continue
                
                # only pads with name send_rtp_src...
                pad_name = pad.get_name()
                if not pad_name.startswith('send_rtp_src'):
                    continue
            
                print("pad: {0:s}".format(pad.name))
                caps = pad.get_current_caps()
                print("  {0:s}".format(caps.to_string()))

                ret, media = GstSdp.SDPMedia.new()
                if ret != GstSdp.SDPResult.OK:
                    print("Error")
                    return
                
                media.set_port_info(port, 1)
                port = port + 2
                media.proto = "RTP/AVP"
        
                ret = GstSdp.SDPMedia.set_media_from_caps(caps, media)
                if ret != GstSdp.SDPResult.OK:
                    print("Error")
                
                sdp_message.add_media(media)
                                
            elif ret==Gst.IteratorResult.DONE:
                break
            elif ret==Gst.IteratorResult.ERROR:
                break
            
        
        print(sdp_message.as_text())
        f = open(sdp_path, 'w')
        f.write(sdp_message.as_text()) 
            
    def on_message(self, bus, msg, user_data):
        t = msg.type

        if t == Gst.MessageType.EOS:
            print("End of stream")
            self.loop.quit()

        elif t == Gst.MessageType.ERROR:
            err, debug = msg.parse_error()
            print("Error: {0:s}".format(err.message))
            self.loop.quit()

        elif t == Gst.MessageType.STATE_CHANGED:
            old, new, pending = msg.parse_state_changed()
            if new == Gst.State.PAUSED:
                if msg.src.name=="bin":
                    print("RtpBin")
                    rtpbin = msg.src
                    self.build_sdp(rtpbin, self.dst_address, self.initial_port, "output.sdp", None)

        elif t == Gst.MessageType.STREAM_STATUS:
            print("Stream status ({0:s})".format(msg.src.name))

        elif t == Gst.MessageType.NEW_CLOCK:
            print("New clock ({0:s})".format(msg.src.name))
            clock = msg.parse_new_clock()
            print("clock time: {0:d}".format(clock.get_time()))

        else:
            # Unhandled message
            print("Received message: src={0} type={1}".format(msg.src.name, Gst.MessageType.get_name(msg.type)))
        
    def __init__(self):
        if len(sys.argv) < 2:
            print("Usage: {0:s} mp4_file".format(sys.argv[0]))
            sys.exit(1)

        # initialisation
        Gst.init()
        
        # main loop
        self.loop = GLib.MainLoop.new(None, False)

        # create the pipeline
        input_path = sys.argv[1]
        self.dst_address = "234.1.2.3"
        self.initial_port = 5004
        self.initial_payload = 96
        
        dst_address = self.dst_address
        rtp_video_payload_type = self.initial_payload
        video_rtp_port = self.initial_port
        video_rtcp_port = video_rtp_port + 1
        rtp_audio_payload_type = rtp_video_payload_type + 1
        audio_rtp_port = video_rtp_port + 2
        audio_rtcp_port = audio_rtp_port + 1
        
        pipeline_str = "rtpbin name=bin \
filesrc location={input_path} ! qtdemux name=demux \
demux.video_0 ! queue ! rtph264pay pt={rtp_video_payload_type} ! bin.send_rtp_sink_0 \
bin.send_rtp_src_0 ! udpsink host={dst_address} port={video_rtp_port} sync=true \
bin.send_rtcp_src_0 ! udpsink host={dst_address} port={video_rtcp_port} sync=false async=false \
demux.audio_0 ! queue ! rtpmp4gpay pt={rtp_audio_payload_type} ! bin.send_rtp_sink_1 \
bin.send_rtp_src_1 ! udpsink host={dst_address} port={audio_rtp_port} sync=true \
bin.send_rtcp_src_1 ! udpsink host={dst_address} port={audio_rtcp_port} sync=false async=false".format(
        input_path=input_path, dst_address=dst_address,
        rtp_video_payload_type=rtp_video_payload_type, video_rtp_port=video_rtp_port, video_rtcp_port=video_rtcp_port,
        rtp_audio_payload_type=rtp_audio_payload_type, audio_rtp_port=audio_rtp_port, audio_rtcp_port=audio_rtcp_port
    )
        print(pipeline_str)
        self.pipeline = Gst.parse_launch(pipeline_str)

        # message handler
        bus = self.pipeline.get_bus()
        #bus.add_watch(GLib.PRIORITY_DEFAULT, self.on_message, None)
        bus.add_signal_watch()
        bus.connect("message", self.on_message, None)

        # set the pipeline to PLAYING state
        self.pipeline.set_state(Gst.State.PLAYING)
        
        # iterate
        print("Running...")
        self.loop.run()

        # out of the main loop, clean up nicely
        print("Returned, stopping playback")
        self.pipeline.set_state(Gst.State.NULL)

if __name__ == '__main__':
    # to be able to use CTRL-C to quit
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    p = Player()
