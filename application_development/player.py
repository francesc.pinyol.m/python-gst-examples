#!/usr/bin/env python3

import sys
import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GObject, GLib

class Player(object):

    def on_message(self, bus, msg, user_data):
        t = msg.type
    
        if (t==Gst.MessageType.EOS):
            print("End of stream")
            self.loop.quit()     
        elif (t == Gst.MessageType.ERROR):
            err, debug = msg.parse_error()
            print("Error: {0:s}".format(err.message))
            self.loop.quit()
        elif (t == Gst.MessageType.QOS):
            live, running_time, stream_type, timestamp, duration = msg.parse_qos()
            print("[QOS] -- live={}, running_time={}, stream_type={}, timestamp={}, duration={}".format(live, running_time, stream_type, timestamp, duration))
            format, processed, dropped = msg.parse_qos_stats()
            print("[QOS]    format={}, processed={}, dropped={}".format(format, processed, dropped))
            jitter, proportion, quality = msg.parse_qos_values()
            print("[QOS]    jitter={}, proportion={}, quality={}".format(jitter, proportion, quality))
        else:
            # unhandled message
            pass

    def __init__(self):
        
        # init GStreamer
        Gst.init(None)

        # create main glib loop
        self.loop = GLib.MainLoop.new(None, False)
        
        # check args
        if len(sys.argv) != 2:
            print("Usage: {0:s} <uri or file>".format(sys.argv[0]))
            sys.exit(1)

        if Gst.Uri.is_valid(sys.argv[1]):
            uri = sys.argv[1]
        else:
            uri = Gst.filename_to_uri(sys.argv[1])
            print("uri: {0:s}".format(uri))
            
        # create a new pipeline to hold the elements
        pipeline_str = "playbin uri={0}".format(uri)
        pipeline = Gst.parse_launch(pipeline_str)
        
        # attach a callback for messages
        bus = pipeline.get_bus()
        bus.add_signal_watch()
        bus.connect("message", self.on_message, None)
        
        # start playing
        pipeline.set_state(Gst.State.PLAYING)
        self.loop.run()
         
        # unset
        pipeline.set_state(Gst.State.NULL)
         
if __name__ == '__main__':
    # to be able to use CTRL-C to quit
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    p = Player()

