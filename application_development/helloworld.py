#!/usr/bin/env python3

import sys
import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib

# https://gstreamer.freedesktop.org/documentation/application-development/basics/helloworld.html

class Player(object):

    def bus_call(self, bus, msg, user_data):
        if (msg.type == Gst.MessageType.EOS):
            print("End of stream")
            self.loop.quit()      
        elif (msg.type == Gst.MessageType.ERROR):
            err, debug = msg.parse_error()
            print("Error: {0:s}".format(err.message))
            self.loop.quit()
        else:
            # Unhandled message
            pass

    def on_pad_added(self, element, pad, data):
        decoder = data

        # We can now link this pad with the vorbis-decoder sink pad
        print("Dynamic pad created, linking demuxer/decoder")

        sinkpad = decoder.get_static_pad("sink")

        pad.link(sinkpad)
        
    def __init__(self):

        # Initialisation
        Gst.init()
        
        self.loop = GLib.MainLoop.new(None, False)

        # Check input arguments
        if len(sys.argv) != 2:
            print("Usage: {0:s} <Ogg/Vorbis filename>".format(sys.argv[0]))
            sys.exit(1)

        # Create gstreamer elements
        pipeline = Gst.Pipeline.new("audio-player")
        source = Gst.ElementFactory.make("filesrc", "file-source")
        demuxer = Gst.ElementFactory.make("oggdemux", "ogg-demuxer")
        decoder = Gst.ElementFactory.make("vorbisdec", "vorbis-decoder")
        conv = Gst.ElementFactory.make("audioconvert", "converter")
        sink = Gst.ElementFactory.make("autoaudiosink", "audio-output")

        if not pipeline or not source or not demuxer or not decoder or not conv or not sink:
            print("One element could not be created. Exiting.")
            sys.exit(1)

        # Set the pipeline

        # we set the input filename to the source element
        source.set_property("location", sys.argv[1])

        # we add a message handler
        bus = pipeline.get_bus()
        bus.add_watch(GLib.PRIORITY_DEFAULT, self.bus_call, None)

        # we add all elements into the pipeline
        # file-source | ogg-demuxer | vorbis-decoder | converter | alsa-output
        pipeline.add(source)
        pipeline.add(demuxer)
        pipeline.add(decoder)
        pipeline.add(conv)
        pipeline.add(sink)

        # we link the elements together
        # file-source -> ogg-demuxer ~> vorbis-decoder -> converter -> alsa-output
        source.link(demuxer)
        demuxer.link(decoder)
        decoder.link(conv)
        conv.link(sink)
        demuxer.connect("pad-added", self.on_pad_added, decoder)

        # note that the demuxer will be linked to the decoder dynamically.
        # The reason is that Ogg may contain various streams (for example
        # audio and video). The source pad(s) will be created at run time,
        # by the demuxer when it detects the amount and nature of streams.
        # Therefore we connect a callback function which will be executed
        # when the "pad-added" is emitted.

        # Set the pipeline to "playing" state
        print("Now playing: {0:s}".format(sys.argv[1]))
        pipeline.set_state(Gst.State.PLAYING)
        
        # Iterate
        print("Running...")
        self.loop.run()

        # Out of the main loop, clean up nicely
        print("Returned, stopping playback")
        pipeline.set_state(Gst.State.NULL)
        
if __name__ == '__main__':
    p = Player()
