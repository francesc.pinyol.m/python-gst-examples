#!/usr/bin/env python3

import sys
import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GObject, GLib

class Player(object):

    def __init__(self):
        
        # init GStreamer
        Gst.init(None)
        self.loop = GLib.MainLoop.new(None, False)
                    
        # build
        pipeline = Gst.Pipeline.new("my-pipeline")
        src = Gst.ElementFactory.make("videotestsrc", "src")
        if not src:
            print("ERROR: could not create 'videotestsrc' element")
            sys.exit(1)

        filter = Gst.ElementFactory.make("capsfilter", "filter")
        # TODO: g_assert

        csp = Gst.ElementFactory.make("videoconvert", "csp")
        if not csp:
            print("ERROR: could not create 'videoconvert' element")
            sys.exit(1)

        sink = Gst.ElementFactory.make("xvimagesink", "sink")
        if not sink:
            sink = Gst.ElementFactory.make("ximagesink", "sink")
            if not sink:
                print("ERROR: could not create 'xvimagesink' nor 'ximagesink' element")
                sys.exit(1)
        
        pipeline.add(src)
        pipeline.add(filter)
        pipeline.add(csp)
        pipeline.add(sink)
        src.link(filter)
        filter.link(csp)
        csp.link(sink)
        filtercaps = Gst.Caps.new_empty_simple("video/x-raw")
        filtercaps.set_value("format", "RGB16")
        filtercaps.set_value("width", 384)
        filtercaps.set_value("height", 288)
        # gst-python must be installed (gi.overrides.Gst.Fraction)
        filtercaps.set_value("framerate", Gst.Fraction(25,1))
        print(filtercaps.to_string())
        
        filter.set_property("caps", filtercaps)
        
        pad = src.get_static_pad("src")
        # TODO
        
        # run
        pipeline.set_state(Gst.State.PLAYING)

        # wait until it's up and running or failed

        print("Running...")
        self.loop.run()
         
        # exit
        pipeline.set_state(Gst.State.NULL)
         
if __name__ == '__main__':
    # to be able to use CTRL-C to quit
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    p = Player()

