#!/usr/bin/env python3

# https://gstreamer.freedesktop.org/documentation/application-development/advanced/buffering.html

import sys
import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib

class Player(object):
    
    def buffer_timeout(self, data):
        #print("[buffer_timeout]")
        pipeline = data
        
        query = Gst.Query.new_buffering(Gst.Format.TIME)
        
        if not pipeline.query(query):
            return True
        
        busy, percent = query.parse_buffering_percent()
        # estimated_total amount of download time remaining in milliseconds
        format, start, stop, estimated_total = query.parse_buffering_range()
        #print("  start {0}, stop {1}, estimated_total {2}".format(start, stop, estimated_total))
        
        if estimated_total == -1:
            estimated_total = 0

        # calculate the remaining playback time
        # position in nanoseconds
        ret, position = pipeline.query_position(Gst.Format.TIME)
        if not ret:
            position = -1
        ret, duration = pipeline.query_duration(Gst.Format.TIME)
        if not ret:
            duration = -1

        if (duration != -1) and (position != -1):
            play_left = (duration - position)/1000000
        else:
            play_left = 0

        print("Message: play_left {0} ms, estimated_total {1} ms, percent {2}%".format(play_left, estimated_total, percent))

        # we are buffering or the estimated download time is bigger than the
        # remaining playback time. We keep buffering.
        self.is_buffering = busy or (estimated_total * 1.1 > play_left)

        if not self.is_buffering:
            pipeline.set_state(self.target_state)

        # when returning False, this callback will not be called again (by the same timeout_add)
        return self.is_buffering
        
    def on_message_buffering(self, bus, message, user_data):
        pipeline = user_data

        # no state management needed for live pipelines
        if self.is_live:
            return
        
        percent = message.parse_buffering()
        
        #print("[on_message_buffering] {0}%".format(percent))

        if percent < 100:
            # buffering busy
            if not self.is_buffering:
                self.is_buffering = True
                if self.target_state == Gst.State.PLAYING:
                    # we were not buffering but PLAYING, PAUSE the pipeline
                    pipeline.set_state(Gst.State.PAUSED)
                        
                             
    def on_message_async_done(self, bus, message, user_data):
        pipeline = user_data
        
        if not self.is_buffering:
            print("  not buffering; setting state to {0}".format(self.target_state))
            pipeline.set_state(self.target_state)
        else:
            GLib.timeout_add(500, self.buffer_timeout, pipeline)
                             
    def __init__(self):
        
        # init GStreamer
        Gst.init()
        self.loop = GLib.MainLoop.new(None, False)
                             
        # make sure we have a URI
        if len(sys.argv) < 2:
            print("Usage: {0:s} URI".format(sys.argv[0]))
            sys.exit(1)

        # set up
        pipeline = Gst.ElementFactory.make("playbin", "pipeline")
        pipeline.set_property("uri", sys.argv[1])
        # flags are explained in "gst-inspect-1.0 playbin"
        pipeline.set_property("flags", 0x697)

        bus = pipeline.get_bus()
        bus.add_signal_watch()
        
        bus.connect("message::buffering", self.on_message_buffering, pipeline)
        bus.connect("message::async-done", self.on_message_async_done, pipeline)

        self.is_buffering = False
        self.target_state = Gst.State.PLAYING
        ret = pipeline.set_state(Gst.State.PAUSED)
        #print("ret: {0}".format(ret))

        self.is_live = False
        if ret == Gst.StateChangeReturn.SUCCESS:
            self.is_live = False
        elif ret == Gst.StateChangeReturn.FAILURE:
            print("WARNING: failed to PAUSE")
            sys.exit(1)
        elif ret == Gst.StateChangeReturn.NO_PREROLL:
            self.is_live = True
            
        # now run
        self.loop.run()

        # also clean up
        pipeline.set_state(Gst.State.NULL)
                             
if __name__ == '__main__':
    # to be able to use CTRL-C to quit
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    p = Player()
