#!/usr/bin/env python3

import sys
import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GObject, GLib

class Player(object):

    def idle_exit_loop(self, user_data):
        self.loop.quit()

        # once
        return False
        
    def on_message(self, bus, msg, user_data):
        t = msg.type
    
        if (t==Gst.MessageType.EOS):
            print("End of stream")
            self.loop.quit()     
        elif (t == Gst.MessageType.ERROR):
            err, debug = msg.parse_error()
            print("Error: {0:s}".format(err.message))
            self.loop.quit()
        else:
            # Unhandled message
            pass

    def cb_typefound(self, element, probability, caps, user_data):
        
        type = caps.to_string()
        print("Media type {0:s} found, probability {1:d}%".format(type, probability))

        # since we connect to a signal in the pipeline thread context, we need
        # to set an idle handler to exit the main loop in the mainloop context.
        # Normally, your app should not need to worry about such things.
        GLib.idle_add(self.idle_exit_loop, None)
        
    def __init__(self):
        
        # init GStreamer
        Gst.init(None)

        self.loop = GLib.MainLoop.new(None, False)
        
        # check args
        if len(sys.argv) != 2:
            print("Usage: {0:s} <filename>".format(sys.argv[0]))
            sys.exit(1)
            
        # create a new pipeline to hold the elements
        pipeline = Gst.Pipeline.new("pipe")

        bus = pipeline.get_bus()
        bus.add_watch(GLib.PRIORITY_DEFAULT, self.on_message, None)

        # create file source and typefind element
        filesrc = Gst.ElementFactory.make("filesrc", "source")
        filesrc.set_property("location", sys.argv[1])
        typefind = Gst.ElementFactory.make("typefind", "typefinder")
        typefind.connect("have-type", self.cb_typefound, None)
        fakesink = Gst.ElementFactory.make("fakesink", "sink")
        
        # setup
        pipeline.add(filesrc)
        pipeline.add(typefind)
        pipeline.add(fakesink)
        filesrc.link(typefind)
        typefind.link(fakesink)
        pipeline.set_state(Gst.State.PLAYING)
        self.loop.run()
         
        # unset
        pipeline.set_state(Gst.State.NULL)
         
         
if __name__ == '__main__':
    # to be able to use CTRL-C to quit
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    p = Player()

