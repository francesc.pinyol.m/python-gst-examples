#!/usr/bin/env python3

# https://gstreamer.freedesktop.org/documentation/application-development/advanced/metadata.html

import sys
import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib

class Info(object):

    def print_one_tag(self, list, tag, user_data):
        res, val = Gst.TagList.copy_value(list, tag)
        print("%s: %s"% (Gst.tag_get_nick(tag), val))

    def on_new_pad(self, src, new_pad, fakesink):
        sinkpad = fakesink.get_static_pad("sink")
        if not sinkpad.is_linked():
            if new_pad.link(sinkpad) != Gst.PadLinkReturn.OK:
                print("Failed to link pads!")
    
    def __init__(self):
        # Initialisation
        Gst.init()

        if len(sys.argv) < 2:
            print("Usage: {0:s} FILE or URI".format(sys.argv[0]))
            sys.exit(1)

        if Gst.Uri.is_valid(sys.argv[1]):
            uri = sys.argv[1]
        else:
            uri = Gst.filename_to_uri(sys.argv[1])
        print("uri: {0:s}".format(uri))
        
        pipe = Gst.Pipeline.new("pipeline")

        dec = Gst.ElementFactory.make("uridecodebin", None)
        dec.set_property("uri", uri)
        pipe.add(dec)

        sink = Gst.ElementFactory.make("fakesink", None)
        pipe.add(sink)

        dec.connect("pad-added", self.on_new_pad, sink)

        pipe.set_state(Gst.State.PAUSED)

        bus = pipe.get_bus()
        while True:
            msg = bus.timed_pop_filtered(Gst.CLOCK_TIME_NONE,
                                         Gst.MessageType.ASYNC_DONE | Gst.MessageType.TAG | Gst.MessageType.ERROR)

            if msg.type != Gst.MessageType.TAG:
                print("got message of type: {0}".format(msg.type))
                break
                
            tags = msg.parse_tag()
            print("Got {0:d} tags from element: {1:s}".format(tags.n_tags(), msg.src.name))
            tags.foreach(self.print_one_tag, None)
            
        pipe.set_state(Gst.State.NULL)
            
if __name__ == '__main__':
    p = Info()
