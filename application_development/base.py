#!/usr/bin/env python3

import sys
import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GObject, GLib

class Player(object):

    def on_message(self, bus, msg, user_data):
        t = msg.type
    
        if (t==Gst.MessageType.EOS):
            print("End of stream")
            self.loop.quit()     
        elif (t == Gst.MessageType.ERROR):
            err, debug = msg.parse_error()
            print("Error: {0:s}".format(err.message))
            self.loop.quit()
        else:
            # unhandled message
            pass

    def __init__(self):
        
        # init GStreamer
        Gst.init(None)

        # create main glib loop
        self.loop = GLib.MainLoop.new(None, False)
        
        # check args
        if len(sys.argv) != 2:
            print("Usage: {0:s} <int_pattern>".format(sys.argv[0]))
            sys.exit(1)
            
        # create a new pipeline to hold the elements
        pipeline = Gst.Pipeline.new("pipeline")

        # attach a callback for messages
        bus = pipeline.get_bus()
        bus.add_watch(GLib.PRIORITY_DEFAULT, self.on_message, None)

        # create elements
        src = Gst.ElementFactory.make("videotestsrc", "src")
        src.set_property("pattern", int(sys.argv[1]))
        sink = Gst.ElementFactory.make("autovideosink", "sink")
        
        # add elements to the pipeline
        pipeline.add(src)
        pipeline.add(sink)

        # link elements
        src.link(sink)
        
        # start playing
        pipeline.set_state(Gst.State.PLAYING)
        self.loop.run()
         
        # unset
        pipeline.set_state(Gst.State.NULL)
         
if __name__ == '__main__':
    # to be able to use CTRL-C to quit
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    p = Player()

