#!/usr/bin/env python3

import sys
import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib
from helper import format_ns


class Player(object):

    def on_message(self, bus, msg, data):
        
        if (msg.type == Gst.MessageType.ERROR):
            err, debug = msg.parse_error()
            print("Error: {0:s}".format(err.message))
            
            self.pipeline.set_state(Gst.State.READY)
            self.loop.quit()
            
        elif (msg.type == Gst.MessageType.EOS):
            # end-of-stream
            print("EOS")
            self.pipeline.set_state(Gst.State.READY)
            self.loop.quit()
            
        elif (msg.type == Gst.MessageType.BUFFERING):
            percent = 0

            # If the stream is live, we do not care about buffering.
            if self.is_live:
                return
            
            percent = msg.parse_buffering()
            print("Buffering ({0:d}%)".format(percent))
            
            # Wait until buffering is complete before start/resume playing
            if (percent < 0):
                self.pipeline.set_state(Gst.State.PAUSED)
            else:
                self.pipeline.set_state(Gst.State.PLAYING)
                
        elif (msg.type == Gst.MessageType.CLOCK_LOST):
            # Get a new clock
            self.pipeline.set_state(Gst.State.PAUSED)
            self.pipeline.set_state(Gst.State.PLAYING)            
            
        elif (msg.type == Gst.MessageType.ASYNC_DONE):
            running_time = msg.parse_async_done()
            #print("ASYNC_DONE running_time: {}".format(running_time))
            # query_duration
            if self.duration == Gst.CLOCK_TIME_NONE:
                ret, duration = self.pipeline.query_duration(Gst.Format.TIME)
                if ret:
                    self.duration = duration
                    print("ret: {}, duration: {}".format(ret, format_ns(duration)))
        else:
            # Unhandled message
            pass

    def cb_check_position(self):
        ret, position = self.pipeline.query_position(Gst.Format.TIME)
        #ret, len = pipeline.query_duration(Gst.Format.TIME)
        #print("{0:f}/{1:f}".format(pos, len))
        if ret:
            print("position: {} / {}".format(format_ns(position), format_ns(self.duration) ))
            if position + self.periodic_check_ms / 1000.0 * Gst.SECOND > self.duration:
                self.pipeline.seek_simple(
                                Gst.Format.TIME, Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT, 0 * Gst.SECOND)

        return True
        
    def __init__(self):
        self.is_live = False
        self.periodic_check_ms = 200

        # media duration (ns)
        self.duration = Gst.CLOCK_TIME_NONE
        
        # Initialize GStreamer
        Gst.init(sys.argv)

        # Build the pipeline
        self.pipeline = Gst.parse_launch("playbin uri=https://www.freedesktop.org/software/gstreamer-sdk/data/media/sintel_trailer-480p.webm")
        bus = self.pipeline.get_bus()

        # Start playing
        ret = self.pipeline.set_state(Gst.State.PLAYING)
        if ret == Gst.StateChangeReturn.FAILURE:
            print("Unable to set the pipeline to the playing state")
            sys.exit(1)
        elif ret == Gst.StateChangeReturn.NO_PREROLL:
            self.is_live = True

        self.loop = GLib.MainLoop.new(None, False)
        GLib.timeout_add(self.periodic_check_ms, self.cb_check_position)

        bus.add_signal_watch()
        bus.connect("message", self.on_message, None)

        self.loop.run()

        # Free resources
        self.pipeline.set_state(Gst.State.NULL)

if __name__ == '__main__':
    d = Player()
