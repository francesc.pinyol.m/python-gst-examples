#!/usr/bin/env python3

import sys
import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib

class Player(object):

    def on_message(self, bus, msg, data):
        
        if (msg.type == Gst.MessageType.ERROR):
            err, debug = msg.parse_error()
            print("Error: {0:s}".format(err.message))
            
            self.pipeline.set_state(Gst.State.READY)
            self.loop.quit()
            
        elif (msg.type == Gst.MessageType.EOS):
            # end-of-stream
            print("EOS")
            self.pipeline.set_state(Gst.State.READY)
            self.loop.quit()
            
        elif (msg.type == Gst.MessageType.BUFFERING):
            percent = 0

            # If the stream is live, we do not care about buffering.
            if self.is_live:
                return
            
            percent = msg.parse_buffering()
            print("Buffering ({0:d}%)".format(percent))
            
            # Wait until buffering is complete before start/resume playing
            if (percent < 0):
                self.pipeline.set_state(Gst.State.PAUSED)
            else:
                self.pipeline.set_state(Gst.State.PLAYING)
                
        elif (msg.type == Gst.MessageType.CLOCK_LOST):
            # Get a new clock
            self.pipeline.set_state(Gst.State.PAUSED)
            self.pipeline.set_state(Gst.State.PLAYING)            
            
        else:
            # Unhandled message
            pass
        
    def __init__(self):
        self.is_live = False
        
        # Initialize GStreamer
        Gst.init(sys.argv)

        # Build the pipeline
        self.pipeline = Gst.parse_launch("playbin uri=https://www.freedesktop.org/software/gstreamer-sdk/data/media/sintel_trailer-480p.webm")
        bus = self.pipeline.get_bus()

        # Start playing
        ret = self.pipeline.set_state(Gst.State.PLAYING)
        if ret == Gst.StateChangeReturn.FAILURE:
            print("Unable to set the pipeline to the playing state")
            sys.exit(1)
        elif ret == Gst.StateChangeReturn.NO_PREROLL:
            self.is_live = True

        self.loop = GLib.MainLoop.new(None, False)

        bus.add_signal_watch()
        bus.connect("message", self.on_message, None)

        self.loop.run()

        # Free resources
        self.pipeline.set_state(Gst.State.NULL)

if __name__ == '__main__':
    d = Player()
