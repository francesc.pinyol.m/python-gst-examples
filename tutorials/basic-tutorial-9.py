#!/usr/bin/env python3

import sys
import gi
gi.require_version('Gst', '1.0')
gi.require_version('GstPbutils', '1.0')
from gi.repository import Gst, GstPbutils, GLib
from gi.repository.GstPbutils import DiscovererAudioInfo, DiscovererVideoInfo

from helper import format_ns

class Discoverer(object):
    
    # Print a tag in a human-readable format (name: value)
    def print_tag_foreach(self, list, tag, depth):        
        res, val = Gst.TagList.copy_value(list, tag)
        print("%s%s: %s"% ((2 * depth) * " ", Gst.tag_get_nick(tag), val))
    
    # Print information regarding a stream
    def print_stream_info(self, info, depth):
        caps = info.get_caps()
        desc = ""
        if (caps):
            if caps.is_fixed():
                desc = GstPbutils.pb_utils_get_codec_description(caps)
            else:
                desc = caps.to_string()
      
        print("{0:s}{1:s}: {2:s}".format(2 * depth * " ", info.get_stream_type_nick(), desc))
    
        tags = info.get_tags()
        if tags:
            print("%sTags:"% (" " * (2 * (depth + 1))))
            tags.foreach(self.print_tag_foreach, depth * 2)

    # Print information regarding a stream and its substreams, if any
    def print_topology(self, info, depth):
        if not info:
            return
        
        self.print_stream_info(info, depth)
        
        next = info.get_next()
      
        if next:
            self.print_topology (next, depth + 1);
        elif isinstance(info, GstPbutils.DiscovererContainerInfo):
            streams = info.get_streams()
            for tmp in streams:
                self.print_topology(tmp, depth + 1)
    
    # This function is called every time the discoverer has information regarding
    # one of the URIs we provided
    def on_discovered(self, discoverer, info, error):
        uri = info.get_uri()
        result = info.get_result()
        if result == GstPbutils.DiscovererResult.URI_INVALID:  
            print("Invalid URI '{0:s}'".format(uri))
        elif result == GstPbutils.DiscovererResult.ERROR:  
            print("Discoverer error: {0:s}".format(error.message))
        elif result == GstPbutils.DiscovererResult.TIMEOUT:
            print("Timeout")
        elif result == GstPbutils.DiscovererResult.BUSY:
            print("Busy")
        elif result == GstPbutils.DiscovererResult.MISSING_PLUGINS:
            print("Missing plugins: {0:s}".format(info.get_misc().to_string()))
        elif result == GstPbutils.DiscovererResult.OK:
            print("Discovered '{0:s}'".format(uri))
            # If we got no error, show the retrieved information
            print("Duration: {0:s}".format(format_ns(info.get_duration())))

            tags = info.get_tags();
            if tags:
                print("Tags:")
                tags.foreach(self.print_tag_foreach, 1)
            
            print("Seekable: {0:s}".format("yes" if info.get_seekable() else "no"))
            print("")

            sinfo = info.get_stream_info()
            if sinfo:
                print("Stream information:")
                self.print_topology(sinfo, 1)
                print("")

            print("Streams:")
            streams = info.get_stream_list()
            for stream in streams:
                if type(stream) is DiscovererAudioInfo:
                    if stream.get_depth() == 0:
                        return None
                    channels = stream.get_channels()
                    sample_rate = stream.get_sample_rate()
                    depth = stream.get_depth()
                    print("  [audio] channels: {0}; sample_rate: {1}; depth: {2}".format(channels, sample_rate, depth))
                elif type(stream) is DiscovererVideoInfo:
                    par = stream.get_par_num() / stream.get_par_denom()
                    width = stream.get_width()
                    height = stream.get_height()
                    is_interlaced = stream.is_interlaced()
                    bitrate = stream.get_bitrate()
                    max_bitrate = stream.get_max_bitrate()
                    if stream.is_image():
                        print("  [image] width: {0}; height: {1}; par: {2}".format(width, height, par))
                    else:
                        fps_num = stream.get_framerate_num()
                        fps_denom = stream.get_framerate_denom()
                        print("  [video] width: {0}; height: {1}; par: {2}; fps: {3}/{4}; interlaced: {5}; bitrate: {6}; max_bitrate: {7}".format(width, height, par, fps_num, fps_denom, is_interlaced, bitrate, max_bitrate))
                    
        if result != GstPbutils.DiscovererResult.OK:
            print("This URI cannot be played")
    
    # This function is called when the discoverer has finished examining
    # all the URIs we provided
    def on_finished(self, discoverer):
        print("Finished discovering")
        self.loop.quit()

    def __init__(self):
        uri = "https://www.freedesktop.org/software/gstreamer-sdk/data/media/sintel_trailer-480p.webm"
        
        # Initialize GStreamer
        Gst.init(sys.argv)
        
        print("Discovering {0:s}".format(uri))
        
        # Instantiate the Discoverer
        discoverer = GstPbutils.Discoverer.new(5 * Gst.SECOND)
        if not discoverer:
            print("Error creating discoverer instance")
            return -1

        # Connect to the interesting signals
        discoverer.connect("discovered", self.on_discovered)
        discoverer.connect("finished", self.on_finished)

        # Start the discoverer process (nothing to do yet)
        discoverer.start()
    
        # Add a request to process asynchronously the URI passed through the command line
        if not discoverer.discover_uri_async(uri):
            print("Failed to start discovering URI {0:s}".format(uri))
            return -1

        # Create a GLib Main Loop and set it to run, so we can wait for the signals
        self.loop = GLib.MainLoop.new(None, False)
        self.loop.run()
        
        # Stop the discoverer process
        discoverer.stop()

if __name__ == '__main__':
    d = Discoverer()
