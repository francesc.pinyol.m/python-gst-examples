#!/usr/bin/env python3

import sys
import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib

if __name__ =="__main__":
    # to be able to use CTRL-C to quit
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    # init GStreamer
    Gst.init(None)
    
    # workaround to avoid "g_array_append_vals: assertion 'array' failed" when importing GES, GstPbutils before Gst.init (using python3)
    gi.require_version('GES', '1.0')
    gi.require_version('GstPbutils', '1.0')
    from gi.repository import GES
    from gi.repository import GstPbutils

    # container profile
    name = "mp4"
    description = "MP4 container profile"
    #container_caps = "video/webm"
    container_caps = "video/quicktime,variant=iso"
    format = Gst.Caps(container_caps)
    preset = None
    container_profile = GstPbutils.EncodingContainerProfile.new(name, description, format, preset)

    # video profile
    #video_caps = "video/x-vp8"
    video_caps = "video/x-h264"
    format = Gst.Caps(video_caps)
    preset = None
    restriction = Gst.Caps("video/x-raw")
    presence = 0 # allow any number of instances of this profile
    video_profile = GstPbutils.EncodingVideoProfile.new(format, preset, restriction, presence)
    container_profile.add_profile(video_profile)

    # audio profile
    #audio_caps = "audio/x-vorbis"
    audio_caps = "audio/mpeg,mpegversion=4"
    format = Gst.Caps(audio_caps)
    preset = None
    restriction = Gst.Caps("audio/x-raw")
    presence = 0 # allow any number of instances of this profile
    audio_profile = GstPbutils.EncodingAudioProfile.new(format, preset, restriction, presence)
    container_profile.add_profile(audio_profile)
    
    # create target with all profiles
    # The name and category can only consist of lowercase ASCII letters for the first character, followed by either lowercase ASCII letters, digits or hyphens (‘-‘).
    name = "mp4target" 
    category = GstPbutils.ENCODING_CATEGORY_DEVICE # "device"
    description = "MP4 (H.264, AAC) target"
    profiles = [container_profile, video_profile, audio_profile]
    target = GstPbutils.EncodingTarget.new(name, category, description, profiles)

    # save target to ~/.local/share/gstreamer-1.0/encoding-profiles/<category>/<name>.gep
    ret = target.save()

    # list targets for all categories
    category = None
    target_list = GstPbutils.encoding_list_all_targets(category)

    print("target_list: {}".format(target_list))
