#!/usr/bin/env python3

import sys
import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib

class Player(object):

    def on_message(self, bus, msg, user_data):
        t = msg.type
    
        if (t==Gst.MessageType.EOS):
            print("End of stream")
            self.loop.quit()     
        elif (t == Gst.MessageType.ERROR):
            err, debug = msg.parse_error()
            print("Error: {0:s}".format(err.message))
            self.loop.quit()
        else:
            # unhandled message
            pass

    def __init__(self):
        
        # check input arguments
        if len(sys.argv) != 4:
            print("Usage: {0:s} <filename or uri> <inpoint> <duration>".format(sys.argv[0]))
            sys.exit(1)
            
        # init GStreamer
        Gst.init(None)

        # workaround to avoid "g_array_append_vals: assertion 'array' failed" when importing GES before Gst.init (using python3)
        gi.require_version('GES', '1.0')
        from gi.repository import GES
        
        # init GES
        GES.init()

        # create main glib loop
        self.loop = GLib.MainLoop.new(None, False)

        # validate input arguments
        if Gst.Uri.is_valid(sys.argv[1]):
            uri = sys.argv[1]
        else:
            uri = Gst.filename_to_uri(sys.argv[1])
        inpoint = float(sys.argv[2])
        duration = float(sys.argv[3])
        print("uri: {0:s}, inpoint: {1}, duration: {2}".format(uri, inpoint, duration))
        
        # create timeline
        timeline = GES.Timeline.new_audio_video()

        # create asset
        asset = GES.UriClipAsset.request_sync(uri)

        # create layer
        layer = timeline.append_layer()

        # put clip in layer
        # start=0.0
        start_on_timeline = 0
        # inpoint=60.0
        start_position_asset = inpoint * Gst.SECOND
        # duration=5.0
        duration = duration * Gst.SECOND 
        clip = layer.add_asset(asset, start_on_timeline, start_position_asset,
                    duration, GES.TrackType.UNKNOWN)

        timeline.commit()

        # create GES pipeline
        pipeline = GES.Pipeline()

        # connect bus messages to callback
        bus = pipeline.get_bus()
        bus.add_signal_watch()
        bus.connect("message", self.on_message, None)

        # add timeline to pipeline
        pipeline.set_timeline(timeline)

        # start playing pipeline
        pipeline.set_state(Gst.State.PLAYING)
        self.loop.run()
        
        # unset
        pipeline.set_state(Gst.State.NULL)

if __name__ =="__main__":
    # to be able to use CTRL-C to quit
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    p = Player()
