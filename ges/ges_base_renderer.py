#!/usr/bin/env python3

import sys
import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib

def format_ns(ns):
    s, ns = divmod(ns, 1000000000)
    m, s = divmod(s, 60)
    h, m = divmod(m, 60)

    return "%u:%02u:%02u.%09u" % (h, m, s, ns)

class Renderer(object):

    def duration_querier(self, pipeline):
        ret, position = pipeline.query_position(Gst.Format.TIME)
        print(format_ns(position))
        return True

    def on_message(self, bus, msg, user_data):
        t = msg.type
    
        if (t==Gst.MessageType.EOS):
            print("End of stream")
            self.loop.quit()     
        elif (t == Gst.MessageType.ERROR):
            err, debug = msg.parse_error()
            print("Error: {0:s}".format(err.message))
            self.loop.quit()
        else:
            # unhandled message
            pass

    def __init__(self):
        
        # check input arguments
        if len(sys.argv) != 5:
            print("Usage: {0:s} <input filename or uri> <inpoint> <duration> <output filename or uri>".format(sys.argv[0]))
            sys.exit(1)
            
        # init GStreamer
        Gst.init(None)

        # workaround to avoid "g_array_append_vals: assertion 'array' failed" when importing GES, GstPbutils before Gst.init (using python3)
        gi.require_version('GES', '1.0')
        gi.require_version('GstPbutils', '1.0')
        from gi.repository import GES
        from gi.repository import GstPbutils

        # init GES
        GES.init()

        # create main glib loop
        self.loop = GLib.MainLoop.new(None, False)

        if Gst.Uri.is_valid(sys.argv[1]):
            input_uri = sys.argv[1]
        else:
            input_uri = Gst.filename_to_uri(sys.argv[1])

        inpoint = float(sys.argv[2])
        duration = float(sys.argv[3])

        if Gst.Uri.is_valid(sys.argv[4]):
            output_uri = sys.argv[4]
        else:
            output_uri = Gst.filename_to_uri(sys.argv[4])

        print("input_uri: {0:s}, inpoint: {1}, duration: {2}, output_uri: {3:s}".format(input_uri, inpoint, duration, output_uri))


        # create timeline
        timeline = GES.Timeline.new_audio_video()

        # create asset
        asset = GES.UriClipAsset.request_sync(input_uri)

        # create layer
        layer = timeline.append_layer()

        # put clip in layer
        # start=0.0
        start_on_timeline = 0
        # inpoint
        start_position_asset = inpoint * Gst.SECOND
        # duration
        duration = duration * Gst.SECOND 
        clip = layer.add_asset(asset, start_on_timeline, start_position_asset,
                    duration, GES.TrackType.UNKNOWN)

        timeline.commit()

        # create GES pipeline
        pipeline = GES.Pipeline()

        # connect bus messages to callback
        bus = pipeline.get_bus()
        bus.add_signal_watch()
        bus.connect("message", self.on_message, None)

        # add timeline to pipeline
        pipeline.set_timeline(timeline)

        # container profile
        name = "mp4"
        description = "MP4 container profile"
        #container_caps = "video/webm"
        container_caps = "video/quicktime,variant=iso"
        format = Gst.Caps.from_string(container_caps)
        preset = None
        container_profile = GstPbutils.EncodingContainerProfile.new(name, description, format, preset)
        
        # video profile
        #video_caps = "video/x-vp8"
        video_caps = "video/x-h264"
        format = Gst.Caps.from_string(video_caps)
        preset = None
        restriction = Gst.Caps.from_string("video/x-raw")
        presence = 0 # allow any number of instances of this profile
        video_profile = GstPbutils.EncodingVideoProfile.new(format, preset, restriction, presence)
        container_profile.add_profile(video_profile)
        
        # audio profile
        #audio_caps = "audio/x-vorbis"
        audio_caps = "audio/mpeg,mpegversion=4"
        format = Gst.Caps.from_string(audio_caps)
        preset = None
        restriction = Gst.Caps.from_string("audio/x-raw")
        presence = 0 # allow any number of instances of this profile
        audio_profile = GstPbutils.EncodingAudioProfile.new(format, preset, restriction, presence)
        container_profile.add_profile(audio_profile)

        # pipeline in render mode
        pipeline.set_render_settings(output_uri, container_profile)
        pipeline.set_mode(GES.PipelineFlags.RENDER)
        
        # progress
        GLib.timeout_add(300, self.duration_querier, pipeline)
        
        # start playing pipeline
        pipeline.set_state(Gst.State.PLAYING)
        self.loop.run()
        
        # unset
        pipeline.set_state(Gst.State.NULL)

if __name__ =="__main__":
    # to be able to use CTRL-C to quit
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    p = Renderer()
