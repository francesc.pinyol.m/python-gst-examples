# python-gst-examples #

## Description ##
Python code related to [GStreamer Documentation](https://gstreamer.freedesktop.org/documentation/)
- [Application Development Manual](https://gstreamer.freedesktop.org/documentation/application-development/index.html)
- [Tutorials](https://gstreamer.freedesktop.org/documentation/tutorials/index.html) (based on https://github.com/gkralik/python-gst-tutorial) 
- GES (GStreamer Editing Services)

(see also: http://www.francescpinyol.cat/gstreamer.html)